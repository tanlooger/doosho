import { nanoid } from 'nanoid';

import redis from './redis';

export default async function createComments(req, res) {
  const { url, text, pid } = req.body


  //const { authorization } = req.headers

  //if (!url || !text || !authorization) {
  if (!url || !text ) {
      return res.status(400).json({ message: 'Missing parameter.' })
  }

  try {
    // verify user token
    //const user = await getUser(authorization)
    //if (!user) return res.status(400).json({ message: 'Need authorization.' })

    // const { name, picture, sub, email } = user

    

    const comment = {
      //id: nanoid(),
      id:nanoid(),
      pid,
      created_at: Date.now(),
      text,
    }



    // // write data
    await redis.lpush(url, JSON.stringify(comment))



    return res.status(200).json({})
  } catch (_) {

    return res.status(400).json({ message: 'Unexpected error occurred.' })
  }
}




