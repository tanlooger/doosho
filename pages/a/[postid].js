import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Script from 'next/script';
//import fs from 'fs'
import path from 'path';

export const config = {
  unstable_runtimeJS: false
}
const booksDirectory = path.join(process.cwd(), 'books')

function Blog({ chapter, book }) {
  const router = useRouter();
  return (
    <>
      <Head>
        <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
        <link rel="canonical" href={"https://doosho.com/a/"+chapter.id} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>{chapter.title+"|读书"}</title>
        <meta name="author" content={chapter.author} />
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="/chapter.css"></link>

        
      </Head>

      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-CRT56P0LEC"
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-CRT56P0LEC');
        `}
      </Script>

        <header>
          <h3><Link href="/">读书</Link></h3>
          <h1>{chapter.title}</h1>
            <span>{chapter.author}</span>&nbsp;&nbsp;
            <time>{chapter.uptime}</time>
        </header>

        <main>

          <article>
            {
              chapter.content.split(/\n\n/).map((p, i) => {
                return <p key={i}>{p}</p>
              })
            }
          </article>
          {/*<pre style={{whiteSpace:'pre-wrap'}}>{chapter.content}</pre>*/}

          <div>
            {book.chapters[0].id == chapter.id ? "" : <Link href={router.basePath+book.chapters[chapter.index-1].id}><a className='prepage'>上一篇</a></Link>}
            {book.chapters[book.chapters.length-1].id == chapter.id ? "" : <Link href={router.basePath+book.chapters[chapter.index+1].id}><a className='nextpage'>下一篇</a></Link>}
          </div>
        </main>

        <footer><Link href="/">读书</Link> © doosho.com</footer>

    </>
  )
}



// This function gets called at build time on server-side.
// It won't be called on client-side.
export async function getStaticProps({params}) {

  // const bookpath = path.join(booksDirectory, 'a')
  // const filepath = path.join(bookpath, params.postid)+".txt"

  // const fileContents = fs.readFileSync(filepath, 'utf8')

  
  // const bookJsonPath = path.join(bookpath, 'book.json')
  // const bookJson = fs.readFileSync(bookJsonPath, 'utf8')
  // const bookJson5 = json5.parse(bookJson)

  const fileContents = await fetch(
    "https://tanlooger.github.io/a/"+params.postid+".txt"
  ).then((response) => {if(response.ok) return response.text()});

  //if (!fileContents) { return {notFound: true} }


  const bookJson5 = await fetch(
    "https://tanlooger.github.io/a/book.json"
  ).then((response) => {

    if(response.status == 200) return response.json()
  });

  //if (!bookJson5) { return {notFound: true} }





  var b = bookJson5.chapters.find(chapter => chapter.id == params.postid)

  b.uptime = new Date(b.date)
  b.uptime = b.uptime.getUTCFullYear() + '-' + (b.uptime.getMonth() + 1) + '-' + b.uptime.getDate()

  b.index = bookJson5.chapters.indexOf(b)
  // b.preid = bookJson5.chapters[b.index-1].id
  // b.nextid = bookJson5.chapters[b.index+1].id
  b.content = fileContents

  return {
    props: {
      chapter: b,
      book: bookJson5
    },
    revalidate: 1000
  }
}



// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// the path has not been generated.
export async function getStaticPaths() {
  // const res = await fetch('https://.../books')
  // const books = await res.json()
  

  var paths = []
    // const bookpath = path.join(booksDirectory, "a")  
    // const bookJsonPath = path.join(bookpath, 'book.json')
    // const bookJson = fs.readFileSync(bookJsonPath, 'utf8')
    // const bookJson5 = json5.parse(bookJson)

    const bookJson5 = await fetch(
      "https://tanlooger.github.io/a/book.json"
    ).then((response) => response.json());
  
      paths.push.apply(paths, bookJson5.chapters.map((chapter)=>({
        params: { postid:chapter.id+"" }
      })))
  

    paths.push.apply(paths, bookJson5.chapters.map((chapter)=>({
      params: { postid:chapter.id+"" }
    })))





  // We'll pre-render only these paths at build time.
  // { fallback: blocking } will server-render pages
  // on-demand if the path doesn't exist.
  return { paths, fallback: 'blocking' }
}

export default Blog