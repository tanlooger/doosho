import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
//import fs from 'fs'
import path from 'path';

export const config = {
  unstable_runtimeJS: false
}
const booksDirectory = path.join(process.cwd(), 'books')

function Blog({ book }) {
  const router = useRouter();
  return (
    <>
      <Head>
        <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
        <link rel="canonical" href="https://doosho.com/a" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>{"文章|读书"}</title>
        <meta name="description" content={book.brief} />
        <meta name="author" content={book.author} />
        <meta name="copyright" content="doosho.com" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="/book.css"></link>

        <script async src="https://www.googletagmanager.com/gtag/js?id=G-CRT56P0LEC"></script>
      <script
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{
                  __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'G-CRT56P0LEC', {
              page_path: window.location.pathname,
            });
          `,
                }}
              />
      </Head>

        <header>
          <h3><Link href="/">读书</Link></h3>
        </header>

        <main>
          {
            book.chapters.map((chapter, key) => 
              <h4 key={key}><a href={router.basePath+"a/"+chapter.id}>{chapter.title}</a></h4>
            )
          }
        </main>

        <footer><Link href="/">读书</Link> © doosho.com</footer>

    </>
  )
}

// This function gets called at build time on server-side.
// It won't be called on client-side.
export async function getStaticProps({params}) {


  // const bookJsonPath = path.join(process.cwd(), 'books/a/book.json')
  // const bookJson = fs.readFileSync(bookJsonPath, 'utf8')
  // const bookJson5 = json5.parse(bookJson)

  const data = await fetch(
    "https://tanlooger.github.io/a/book.json"
  ).then((response) => response.json());


  return {
    props: {
      book: data
    },
    revalidate: 1000
  }

  const filenames = fs.readdirSync(path.join(booksDirectory, params.bookid))

  const books = filenames.map(filename => {
    const filePath = path.join(booksDirectory, filename)
    if(fs.statSync(filePath).isFile){
      const fileContents = fs.readFileSync(filePath, 'utf8')

      // Generally you would parse/transform the contents
      // For example you can transform markdown to HTML here
  
      return {
        filename,
        content: fileContents,
      }
    }
  })


  // By returning { props: books }, the Blog component
  // will receive `books` as a prop at build time
  return {
    props: {
      books,
    },
  }
}




export default Blog