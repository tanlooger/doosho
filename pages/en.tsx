import {
  useEffect,
  useState,
} from 'react';

import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';

import styles from '../styles/Home.module.css';

// export const config = {
//   unstable_runtimeJS: false
// }

const Home: NextPage = ({bookss, lang}:any) => {
  const [books, setBooks]:any = useState(bookss)

  
  useEffect(()=>{
    //onSubmit(null)
  }, [setBooks])



  return (
    <>
      <Head>
        <title>读书</title>
        <meta name="description" content="现在是靠知识的年代，多读书，少刷小视频。来读小说，小说也是书，可以提高想象力。" />
        <link rel="icon" href="/book.png" />
      </Head>

      <header className={styles.header}>
        <h1><Link href='/'>读书</Link></h1>
        <form action='/s' >
          <input name='wd' required placeholder='读书书，读读书，搜书书……'/>
          <button type='submit'>搜书</button>
        </form>
      </header>


      <main className={styles.main} >
        



        <div>



          {
            books.map((val:any, i:number)=>(
              <section  key={i} className={styles.card}>
              
            <h2><Link href={lang+'/'+val.bookid} ><a dangerouslySetInnerHTML={{__html: val.title}} /></Link></h2>
            

            <p dangerouslySetInnerHTML={{__html: val.description}}/>

            </section>
          
            ))
          }
        </div>
      </main>

      <footer className={styles.footer}>
        多读书才有希望成为下一个马斯克。不！不！格局小了，要超过他……
      </footer>
    </>
  )
}


export async function getStaticProps({params}:any) {

  //const bookss = require('../books/cn/books.json')

  const bookss = await fetch(`https://tanlooger.github.io/books/en/books.json`).then((response)=>{
    return response.json()
  })

  return {
    props: {
      lang: 'en',
      bookss,
    },
    revalidate: 10, // In seconds
  }
}

export default Home














