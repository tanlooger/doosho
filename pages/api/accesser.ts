import type {
  NextApiRequest,
  NextApiResponse,
} from 'next';

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { Octokit } from '@octokit/rest';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {

  const forwarded = req.headers['x-forwarded-for'];
  const ip = typeof forwarded === 'string' ? forwarded.split(/, /)[0] : req.socket.remoteAddress;


  const gistWdUrl = 'https://gist.githubusercontent.com/tanlooger/'+process.env.GIST_ACCESS_LOG+'/raw'

  var searchHistory = await fetch(gistWdUrl).then((r)=>r.json())

  searchHistory.push([new Date().toLocaleString('zh-CN'), ip, req.headers.referer])

  const octokit = new Octokit({
    auth: process.env.GIST_TOKEN
  });
  
  octokit.request('PATCH /gists/'+process.env.GIST_ACCESS_LOG, {
    files: {
      'doosho-accesser-log.json': {
        content: JSON.stringify(searchHistory)
      }
    }
  })

  res.status(200).json({})
}
