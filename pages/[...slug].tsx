import {
  ReactElement,
  useEffect,
} from 'react';

import matter from 'gray-matter';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import path from 'path';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';
import remarkGfm from 'remark-gfm';

import BookLayout from '../components/booklayout/book-layout';
import styles from '../components/booklayout/book-layout.module.css';

// export const config = {
//   unstable_runtimeJS: false
// }


export default function Book({content, data, bread, prevpage, nextpage}:any) {

  var titlestr = ''

  bread.map((v:any, i:any)=>{
    titlestr = v.title+'|'+titlestr
  })
  titlestr = titlestr.slice(0, -1)
  const router = useRouter()


  useEffect(()=>{



    // const iframes = document.getElementsByTagName('iframe')


    // const iframe = iframes[0]

    // if(iframe){
      
    //   iframe.onblur = (e)=>{
    //     iframe.focus()
    //   }
    // }

    //fetch('/api/accesser')
    window.onload = ()=>{
      setInterval(()=>{
      }, 1000)
    }
  }, [])

  return (
<>
    <Head>
    <title>{titlestr}</title>
    </Head>
      <ul className={styles.breadcrumb} style={{paddingLeft:0}}>
              {
                
                bread.map((v:any, i:any)=>{
                  if(i == bread.length-1){
                    return <li key={i}><span>{v.title}</span></li>
                  }
                  return <li key={i}><Link href={v.slug}><a>{v.title.trim()}</a></Link></li>
                })
              }
            </ul>
            <h1 style={{textAlign:'center'}}>{data.title}</h1>
          <ReactMarkdown rehypePlugins={[rehypeRaw, remarkGfm]}>{content}</ReactMarkdown>
          <hr style={{margin:'2rem 0'}} />
          <p style={{display:'flex', justifyContent:'space-between', flexFlow:'wrap'}}>
          {prevpage ? <Link href={prevpage.slug}><a>{prevpage.title}</a></Link> : <span>这是第一篇</span>}
          {nextpage ? <Link href={nextpage.slug}><a>{nextpage.title}</a></Link> : <span>这是最后一篇</span>}
          </p>
          {/* <BookComment commentss={commentss} /> */}
          </>
  )
}

export async function getStaticProps({params}:any) {
  const Lang = params.slug[0]
  const bookid = params.slug[1]
  const slug = params.slug.join('/')

  if (!Lang || !bookid || isNaN(bookid)) { return {notFound: true} }

  var mdxpath

  const bookjson = await fetch(`${process.env.DB_HOST}/books/${Lang}/${bookid}/book.json`).then((response)=>{
    if(response.ok) return response.json()
  })


  //if (!bookjson) { return {notFound: true} }


  mdxpath = path.join.apply(null, [path.join(process.env.DB_HOST!, 'books'), ...params.slug])


  var bookmap = new Map<string, string>()
  var slugs: any[] = []
  var titles: any[] = []
  var currentindex = 0
  let childlist = '\n\n'

  const flat = (post:any)=>{
    bookmap.set(post.slug, post.title)
    slugs.push(post.slug)
    titles.push(post.title)
    if(post.slug === params.slug.join('/')){
      currentindex = slugs.length-1
    }
    if(post.child){
      if(post.slug == slug){
        console.log(slug)
        post.child.map((p:any, index:number)=>{
          childlist += `[${p.title}](/${p.slug})\n\n`

          flat(p)
        })
      }else{
        post.child.map((p:any, index:number)=>{
          flat(p)
        })
      }


    }
  }



  flat(bookjson)


  const source = await fetch(`${mdxpath}.mdx`).then((response)=>{
    if(response.ok) return response.text()
  })

  //if (!source) { return {notFound: true} }

  let { content, data } = matter(source!)

  content += childlist

  console.log(childlist)


  let bread:any = []
  const t = bookmap.get(params.slug.join('/'))
  bread.unshift({slug:'/'+params.slug.join('/'), title:data.title?data.title:t?t:''})
  params.slug.pop()
  while(params.slug.length>1){
    const t = bookmap.get(params.slug.join('/'))
    bread.unshift({slug:'/'+params.slug.join('/'), title:t?t:data.title})
    params.slug.pop()
  }
  bread.unshift({slug:Lang=='cn'?'/':'/en', title:Lang=='cn'?'读书':'doosho'})

  const prevpage = currentindex ? {slug: '/'+slugs[currentindex-1], title: titles[currentindex-1]} : null
  const nextpage = currentindex < slugs.length-1 ? {slug: '/'+slugs[currentindex+1], title: titles[currentindex+1]} : null


  return {
    props: {
      bookid,
      lang: Lang,
      content,
      data,
      bread,
      bookjson,
      prevpage,
      nextpage,
    },
    revalidate: 10, // In seconds
  }
}




export async function getStaticPaths() {

  const books = await fetch(`${process.env.DB_HOST}/books/cn/books.json`).then((response)=>{
    return response.json()
  })


  var allpaths:any[] = []

  await books.map(async (book:any, index:number)=>{
    //const bookjson = await require(`../books/${Lang}/${book.bookid}/book.json`)

    const bookjson = await fetch(`${process.env.DB_HOST}/books/cn/${book.bookid}/book.json`).then((response)=>{
      return response.json()
    })
    const p = printMenu(bookjson, [])
    allpaths = allpaths.concat(p)
  })


  const books2 = await fetch(`${process.env.DB_HOST}/books/en/books.json`).then((response)=>{
    return response.json()
  })


  await books2.map(async (book:any, index:number)=>{
    //const bookjson = await require(`../books/${Lang}/${book.bookid}/book.json`)

    const bookjson = await fetch(`${process.env.DB_HOST}/books/en/${book.bookid}/book.json`).then((response)=>{
      return response.json()
    })
    const p = printMenu(bookjson, [])
    allpaths = allpaths.concat(p)
  })


  const allpath = allpaths.map((slug, i)=>({ params: { slug: slug.split('/') }}))




  const paths = [
    {params: {slug: ['1']}},
    // {params: {slug: ['1','1']}},
    // {params: {slug: ['1','1', '1']}},
  ]
  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths:allpath, fallback: 'blocking' }
}


Book.getLayout = function getLayout(page: ReactElement) {
  return (
    <BookLayout>
      {page}
    </BookLayout>
  )
}





function printMenu(menu:any, pathss:string[]):any{
  if(!menu) return "";
  for(var key in menu)
  {
    if(key == 'slug'){
      pathss.push(menu.slug)
    }else if(key=='child'){
      menu.child.map((v:any, i:any)=>printMenu(v, pathss))
    }
  }


  return pathss;
}

