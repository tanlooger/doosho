import matter from 'gray-matter';
import Head from 'next/head';
import Link from 'next/link';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

export const config = {
  unstable_runtimeJS: false
}


export default function Post({ postid, content, data }:any) {

  return (
    <>
      <Head>
        <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
        <link rel="canonical" href={"https://doosho.com/a/"+postid} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>{data.title+"|读书"}</title>
        <link rel="icon" href="/favicon.ico" />

        
      </Head>

        <header>
          <h3><Link href="/">读书</Link></h3>
          <h1>{data.title}</h1>
            <time>{data.uptime}</time>
        </header>

        <main>
          <article>
            <ReactMarkdown rehypePlugins={[rehypeRaw]}>{content}</ReactMarkdown>
          </article>

        </main>

        <footer><Link href="/">读书</Link> © doosho.com</footer>

    </>
  )
}

export async function getStaticProps({params}:any) {
  const fileContents = await fetch(
    "https://tanlooger.github.io/b/"+params.postid+".mdx",
    {
      headers: {
        'Content-Type': 'application/text'
      },
    }
  ).then((response) => response.text());



  const { content, data } = matter(fileContents.toString())
  //const { content, data } = matter('---\ntitle: Home\n---\n\n# Other stuff')

  



  return {
    props: {
      postid:params.postid, content, data
    },
    revalidate: 10, // In seconds
  }
}




export async function getStaticPaths() {

  var paths: any[] = []

    const bookJson5 = await fetch( "https://tanlooger.github.io/b/book.json")
    .then((response) =>  response.json());
  
      paths.push.apply(paths, bookJson5.map((chapter: { id: string; })=>({
        params: { postid:chapter.id+"" }
      })))
  



  return { paths, fallback: 'blocking' }
}









