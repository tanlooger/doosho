import { useState } from 'react';

import type { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { Octokit } from '@octokit/rest';

import styles from '../styles/Home.module.css';

export const config = {
  unstable_runtimeJS: false
}

const Home: NextPage = ({bookss, wd}:any) => {
  const [books, setBooks]:any = useState(bookss)

  //const books = require('../books/cn/books.json')
  
  
  // const onSubmit = async (event:any) => {
  //   setBooks(require('../books/cn/books.json'));
  //   if(!event)return
  //   event.preventDefault();

  //   //var searchValue = event.target[0].value.split('').join('|')
  //   const searchValue = event.target[0].value

  //   //alert(searchValue)

  //   fetch('/api/books?words='+searchValue).then(res=>res.json()).then(async json=>{
  //     var searchBooks:any = []
    
  //     json.map((book:any, i:any)=>{
  //       var tmpbook:any = null
  //       if(book.title.search(searchValue) >= 0){
  //         tmpbook = book
  //         tmpbook.title = book.title.replaceAll(searchValue, '<mark>'+searchValue+'</mark>')
  //       }
  //       // if(book.description.search(searchValue) >= 0){
  //       //   if(!tmpbook)tmpbook = book
  //       //   tmpbook.description = book.description.replace(searchValue, `<mark>${searchValue}</mark>`)
  //       // }
  
  //       if(tmpbook)searchBooks.push(tmpbook)
  //     })


  //     await setBooks([])
  //     await setBooks(searchBooks)
  //     searchBooks = []
  //   })
 
  // };

  
  // useEffect(()=>{
  //   //onSubmit(null)
  // }, [setBooks])



  return (
    <>
      <Head>
        <title>读书</title>
        <meta name="description" content="现在是靠知识的年代，多读书，少刷小视频。来读小说，小说也是书，可以提高想象力。" />
        <link rel="icon" href="/book.png" />
      </Head>

      <header className={styles.header}>
        <h1><Link href='/'>读书</Link></h1>
        <form action='/s' >
          <input name='wd' defaultValue={wd} required placeholder='读书书，读读书，搜书书……'/>
          <button type='submit'>搜书</button>
        </form>
      </header>


      <main className={styles.main} >
        



        <div>



          {
            books.map((val:any, i:number)=>(
              <section  key={i} className={styles.card}>
                          
            <h2><Link href={val.bookid} ><a target="_blank" dangerouslySetInnerHTML={{__html: val.title}} /></Link></h2>

            <p dangerouslySetInnerHTML={{__html: val.description}}/>

            </section>
          
            ))
          }
        </div>
      </main>

      <footer className={styles.footer}>
        多读书才有希望成为下一个马斯克。不！不！格局小了，要超过他……
      </footer>
    </>
  )
}



export async function getServerSideProps({req, res, query, resolvedUrl, locales, locale, defaultLocale}:any) {
  const router = useRouter

  const wd = query.wd
  const url = require('url');
  const referer = url.parse(req.headers.referer, true);

  const forwarded = req.headers['x-forwarded-for'];
  const ip = typeof forwarded === 'string' ? forwarded.split(/, /)[0] : req.socket.remoteAddress;









  const books = await fetch(`https://tanlooger.github.io/books/cn/books.json`).then((response)=>{
    return response.json()
  })


  var returnbooks:any = []
  books.map((book:any, index:number)=>{
    const a = wd.split('').join('|')
    const reg = new RegExp(a, 'gi')

    if(reg.test(book.title)){
      book.title = book.title.replace(reg, (matched: any)=>{
        return `<mark>${matched}</mark>`
      })
      if(!isNaN(book.bookid)) book.bookid = 'cn/'+book.bookid
      returnbooks.push(book)
    }
  })

  //const books2 = await require('../books/en/books.json')

  const books2 = await fetch(`https://tanlooger.github.io/books/en/books.json`).then((response)=>{
    return response.json()
  })

  books2.map((book:any, index:number)=>{
    const a = wd.split('').join('|')
    const reg = new RegExp(a, 'gi')

    if(reg.test(book.title)){
      book.title = book.title.replace(reg, (matched: any)=>{
        return `<mark>${matched}</mark>`
      })
      if(!isNaN(book.bookid)) book.bookid = 'en/'+book.bookid
      returnbooks.push(book)
    }
  })



  // write search history to gist
  if(wd != referer.query.wd && referer.hostname==process.env.HOST){
    const gistWdUrl = 'https://gist.githubusercontent.com/tanlooger/'+process.env.GIST_SEARCH_WD+'/raw'

    var searchHistory = await fetch(gistWdUrl).then((r)=>r.json())
  
    searchHistory.push([new Date().toLocaleString('zh-CN'), ip, wd])
  
    const octokit = new Octokit({
      auth: process.env.GIST_TOKEN
    });
    
    octokit.request('PATCH /gists/'+process.env.GIST_SEARCH_WD, {
      files: {
        'doosho-search-keywords.json': {
          content: JSON.stringify(searchHistory)
        }
      }
    })
  }


  

  



  return {
    props: {
      bookss: returnbooks,
      wd,
    }
  }
}

export default Home














