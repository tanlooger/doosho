import { useState } from 'react';

import { useRouter } from 'next/router';
import useSWR from 'swr';

export default function useComments() {
  const [text, setText] = useState('')
  const [pid, setPid] = useState('')
  const url = `${process.env.HOST}/${useRouter().asPath}`

  //const [url, setUrl] = useState(null)

  const fetcher = async (...args:any) => {
    const res = await fetch(args);
    const jsondata = await res.json();
    return jsondata
  }

  const { data: comments, mutate } = useSWR(
    () => {

      const query = new URLSearchParams({ url })

      const q = `/api/comment?${query.toString()}`
      return q
    },
    fetcher
  )

  // useEffect(() => {
  //   const url = window.location.origin + window.location.pathname
  //   setUrl(url)
  // }, [])

  const onSubmit = async (e:any) => {
    e.preventDefault()

    const text = e.target[0].value
    const pid = e.target[1].value

    try {
      await fetch('/api/comment', {
        method: 'POST',
        body: JSON.stringify({ url, text, pid }),
        headers: {
          Authorization: 'token',
          'Content-Type': 'application/json',
        },
      })
      setText('')
      await mutate()
    } catch (err) {
    }
  }



  return { comments, onSubmit }
}
