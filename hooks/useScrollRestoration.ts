/**
 * Based on https://gist.github.com/claus/992a5596d6532ac91b24abe24e10ae81
 * - see https://github.com/vercel/next.js/issues/3303#issuecomment-628400930
 * - see https://github.com/vercel/next.js/issues/12530#issuecomment-628864374
 */
import { useEffect } from 'react';

import Router from 'next/router';

function saveScrollPos(asPath: string, element:HTMLDivElement) {
   sessionStorage.setItem(
     `scrollPos:${asPath}`,
     //JSON.stringify({ x: window.scrollX, y: window.scrollY })
     JSON.stringify({ x: element.scrollLeft, y: element.scrollTop })
   )
 }
 
 function restoreScrollPos(asPath: string, element:HTMLDivElement) {
   const json = sessionStorage.getItem(`scrollPos:${asPath}`)
   const scrollPos = json ? JSON.parse(json) : undefined
   if (scrollPos) {
     //window.scrollTo(scrollPos.x, scrollPos.y)
     element.scrollTo(scrollPos.x, scrollPos.y)
   }
 }
 
 export function useScrollRestoration(asPath:string, element:HTMLDivElement) {
   useEffect(() => {
    if(!element)return
    element.scrollTo(0, 0)
     if (!('scrollRestoration' in window.history)) return
     let shouldScrollRestore = false
     window.history.scrollRestoration = 'manual'
     restoreScrollPos(asPath, element)
 
     const onBeforeUnload = (event: BeforeUnloadEvent) => {
       saveScrollPos(asPath, element)
       delete event['returnValue']
     }
 
     const onRouteChangeStart = () => {
       saveScrollPos(asPath, element)
     }
 
     const onRouteChangeComplete = (url: string, element:HTMLDivElement) => {
       if (shouldScrollRestore) {
         shouldScrollRestore = false
         /**
          * Calling with relative url, not expected asPath, so this
          * will break if there is a basePath or locale path prefix.
          */
         restoreScrollPos(url, element)
       }
     }
 
     window.addEventListener('beforeunload', onBeforeUnload)
     Router.events.on('routeChangeStart', onRouteChangeStart)
     Router.events.on('routeChangeComplete', onRouteChangeComplete)
     Router.beforePopState(() => {
       shouldScrollRestore = true
       return true
     })
 
     return () => {
       window.removeEventListener('beforeunload', onBeforeUnload)
       Router.events.off('routeChangeStart', onRouteChangeStart)
       Router.events.off('routeChangeComplete', onRouteChangeComplete)
       Router.beforePopState(() => true)
     }
   }, [asPath, element])
 }