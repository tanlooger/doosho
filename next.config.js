/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n: {
    locales: ["zh-CN", "en-US"],
    defaultLocale: "zh-CN",
  }
}

module.exports = nextConfig




// const withMDX = require('@next/mdx')({
//   extension: /\.mdx?$/,
//   options: {
//     remarkPlugins: [],
//     rehypePlugins: [],
//     // If you use `MDXProvider`, uncomment the following line.
//     // providerImportSource: "@mdx-js/react",
//   },
// })
// module.exports = withMDX({
//   // Append the default value with md extensions
//   pageExtensions: ['ts', 'tsx', 'js', 'jsx', 'md', 'mdx'],
// })

// module.exports = {
//   experimental: {
//     async headers() {
//       return [
//         {
//           source: '/(.*)?',
//           headers: [
//             {
//               key: 'X-Frame-Options',
//               value: 'SAMEORIGIN'
//             }
//           ]
//         }
//       ]
//     },
//     // temporary until https://github.com/zeit/next.js/pull/11755
//     async rewrites() {
//       return [
//         {
//           source: '/:path*',
//           destination: '/:path*'
//         }
//       ]
//     }
//   }
// }
