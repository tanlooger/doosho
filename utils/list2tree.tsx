



const node=[
  {id:1,pid:0,name:"吉林省"},
  {id:2,pid:1,name:"长春市"},
  {id:3,pid:1,name:"松原市"},
  {id:4,pid:0,name:"辽宁省"},
  {id:5,pid:4,name:"沈阳市"},
  {id:6,pid:3,name:"扶余县"}
]
/*
通过定义map，key当前对象id，  value该对象
遍历集合，得到对象顶级节点放到集合中返回
不是顶级的就是当前对象的子节点，将对象放到该节点下，这一段比较抽象
*/

export function list2tree(node:any){


  if(!node)return
  node.forEach(function(it:any){
    delete it.child;
  })
  // 定义map/
  var map:any = [];
  // 这里可以重构数据类型，放回字段值
  node.forEach(function(it:any){
    it.level = 0;
    map[it.id]=it;
  })

  // 定义返回集合
  var val:any=[];
  var maxid = 0;
  node.forEach(function(it:any){
    if(maxid < it.id)maxid=it.id
    var parent = map[it.pid];
    if(parent){
      it.level = parent.level
      it.level++
      // 有数据说明不是顶级节点，将数据放到该 children 子节点下
      ((parent.child) || (parent.child=[])).push(it);
    }else{
      // 没有数据说明是顶级节点放到val中
      val.push(it);
    }
  });

  return val;
}


