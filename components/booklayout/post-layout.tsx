import {
  useEffect,
  useState,
} from 'react';

import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Script from 'next/script';
import NextNProgress from 'nextjs-progressbar';

import styles from './book-layout.module.css';
import BookSidebar from './book-sidebar';

export default function PostLayout({children}:any) {
  const [isOpened, setOpened] = useState(true);
  const [isMobile, setIsMobile] = useState(false);
  const toggleDrawer = () => {
    setOpened((prev) => !prev);
  };

  const router = useRouter()


  var titlestr = ''

  children.props.bread.map((v:any, i:any)=>{
    titlestr = v.title+'|'+titlestr
  })

  useEffect(()=>{
    

    setTimeout(()=>{
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        setOpened(false)
        setIsMobile(true)
       }
    }, 100)
    window.onload = ()=>{
      setTimeout(()=>{
        
      }, 100)
    }


  },[])

  return (
    <>
      <NextNProgress />
      <Head>
        <title>{titlestr+'读书'}</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossOrigin="anonymous"></link>
                
      </Head>
      <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossOrigin="anonymous"></Script>


      {router.isFallback ? (
        <div>Loading…</div>
      ):(
      <div style={{display:'flex'}}>
        <aside className={styles.aside}  style={{ width:isOpened?isMobile?'100vw':'auto':0}}>
          <span className={styles.sidedrawer} hidden={isOpened} style={{position:'fixed',cursor:'pointer',margin:'3px',opacity:'0.3'}} onClick={toggleDrawer}>☰</span>
          <span className={styles.sidedrawer} style={{cursor:'pointer',float:'right',top:5,position:'sticky',marginRight:'0.5rem'}} onClick={toggleDrawer}>❌</span>
          <BookSidebar bookid={children.props.bookid} lang={children.props.lang} bookjson={children.props.bookjson}/>
        </aside>
        <div className={styles.trunk} >
          <header className={styles.header}>
            {/* <BookSearch bookid={children.props.bookid} lang={children.props.lang} /> */}
          </header>
          <main>
          <ul className={styles.breadcrumb}>
              {
                
                children.props.bread.map((v:any, i:any)=>{
                  if(i == children.props.bread.length-1){
                    return <li key={i}><span>{v.title}</span></li>
                  }
                  
                  return <li key={i}><Link href={'/'+v.slug}  >{v.title}</Link> </li>
                })
              }
            </ul>
            <article className={styles.article}>
              {children}
            </article>
          </main>
          <footer></footer>
        </div>
      </div>)
      }

      
    </>
  )
}
  
