import ClientComment from '../../hooks/client-comment';
import { list2tree } from '../../utils/list2tree';
import styles from './book-comment.module.css';

function BookComment({commentss}:any) {
  
  var {comments} = ClientComment()


  //comments = commentss


  const menus = commentss
  return (
    <div>
      <CommentForm pid='0' />

      <div>
          <MenuList menus={list2tree(comments?comments:commentss)} />
      </div>

    </div>
  )
}


function MenuList({ menus }: any){

  if(!menus)return null;

  return (
    <ul style={{marginLeft: '1rem', listStyleType: 'none'}}>
      {menus.map((menu: any)=>(<MenuItem menu={menu} key={menu.id} />))}
    </ul>
  )
}

function MenuItem({menu}: any){

  if(!menu)return null

  const onClick = (e:any)=>{
    document.getElementById(styles.active)?.removeAttribute('id');
    e.currentTarget.nextElementSibling!.id=styles.active;

    document.getElementById(styles.button)?.removeAttribute('id');
    e.currentTarget.id = styles.button

    
    //e.currentTarget.style.display = 'block';

  }

  //const op = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  const op:any = { year: 'numeric', month: 'long', day: 'numeric' };
  const dd = new Date(menu.created_at).toLocaleDateString('zh-CN', op)

  return(
    <li key={menu.id}>
      <time>{ dd }</time>
      <div>{menu.text}<button onClick={(e)=>{onClick(e);}} >回复</button>
        <div className={styles.comment}>
          <CommentForm pid={menu.id} />
        </div>
      </div>
      


      {
        menu.child ? <MenuList menus={menu.child} /> : null
      }
    </li>
  )
}




function CommentForm({pid}:any){

  const { onSubmit } = ClientComment()

  
  
  return (<>
      <form onSubmit={onSubmit} >
        <textarea placeholder='上面的东西读完了吗？有啥想法？' style={{width:'15rem', height:'5rem'}}/>
        <input name='pid' type='hidden' value={pid} />
        <button>发送</button>
      </form>
  </>
  )
}







export default BookComment
