import {
  useEffect,
  useState,
} from 'react';

import axios from 'axios';
import Link from 'next/link';
import { createRoot } from 'react-dom/client';

const fetcher = (args: RequestInfo) => fetch(args).then((res) => res.json())



export default function BookSearch({bookid, lang}:any) {
  const [datas, setDatas] = useState<any[]>([]);
  const [word, setWord] = useState('');
  let token:any;
  



  const onChangeHandler = async (e:any) => {
    e.preventDefault()

    if(e.target.value.length <2){
      setDatas([])
      setWord('')
      return
    }

    if(token){
      // Cancel the previous request before making a new request
      token.cancel()
    }
    token = axios.CancelToken.source()

    try {
      setWord(e.target.value)
      const res = await axios('/api/search?word='+e.target.value+'&bookid='+bookid+'&lang='+lang, {cancelToken: token.token})
      const data = await res.data
      setDatas(data)
      //setDatas(null)




    } catch (err) {
    }
  }





  
  return (
    <div>



<div>
    <div>
            <input onFocus={e => onChangeHandler(e)} onChange={e => onChangeHandler(e)} onBlur={e=>{setDatas([]);setWord('')}} type="text" style={{width:'20rem', height:'2rem'}} placeholder="书内搜索..." />
        {
          datas && datas.length>0 ? <SearchList datas={datas} word={word} />
        : null}
    </div>
</div>
    </div>

  )
}




function SearchList({datas, word}:any){
  useEffect(() => {
    var ss = document.getElementById("searchlist")
    if(ss){
      const root = createRoot(ss);
      // root.render(
      //   <Highlighter
      //     searchWords={[word]}
      //     textToHighlight={ss.innerHTML?ss.innerHTML:''}
      //   />
      // );
    }
  }, [word]); // <-- empty array means 'run once'



  if(datas.length <= 0)return <></>








  return <ul id='searchlist' >
            
  {datas.map((v:any, i:any)=>(
    
      v.data.map((vv:any, ii:any)=>(
        <li key={i.toString()+ii.toString()} >


        <Link href={v.link}><a  dangerouslySetInnerHTML={{__html: vv.replace(word, `<mark>${word}</mark>`)}} /></Link>
    
  </li>
      ))
    

  

  ))
  
 

  
  }

</ul> 
}