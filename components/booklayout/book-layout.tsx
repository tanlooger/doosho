import {
  useEffect,
  useRef,
  useState,
} from 'react';

import Head from 'next/head';
import { useRouter } from 'next/router';
import Script from 'next/script';
import NextNProgress from 'nextjs-progressbar';

import { useScrollRestoration } from '../../hooks/useScrollRestoration';
import styles from './book-layout.module.css';
import BookSidebar from './book-sidebar';

export default function BookLayout({children}:any) {
  const [isOpened, setOpened] = useState(true);
  const [isMobile, setIsMobile] = useState(false);
  const toggleDrawer = () => {
    setOpened((prev) => !prev);
  };

  const router = useRouter()


  var titlestr = ''

  children.props.bread.map((v:any, i:any)=>{
    titlestr = v.title+'|'+titlestr
  })

  useEffect(()=>{
    

    setTimeout(()=>{
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        setOpened(false)
        setIsMobile(true)
       }
    }, 100)
    window.onload = ()=>{
      setTimeout(()=>{
        
      }, 100)
    }


  },[])

  return (
    <>

      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-CRT56P0LEC"
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-CRT56P0LEC');
        `}
      </Script>
      
      <NextNProgress />
      <Head>
        <title>{titlestr+'读书'}</title>
                
      </Head>


      {router.isFallback ? (
        <div>Loading…</div>
      ):(
      <div style={{display:'flex'}}>
        <aside className={styles.aside}  style={{ width:isOpened?isMobile?'100vw':'auto':0}}>
          <span className={styles.sidedrawer} hidden={isOpened} style={{position:'fixed',cursor:'pointer',margin:'3px',opacity:'0.3'}} onClick={toggleDrawer}>☰</span>
          <span className={styles.sidedrawer} style={{cursor:'pointer',float:'right',top:5,position:'sticky',marginRight:'0.5rem'}} onClick={toggleDrawer}>❌</span>
          <BookSidebar bookid={children.props.bookid} lang={children.props.lang} bookjson={children.props.bookjson}/>
        </aside>

        <Trunk>{children}</Trunk>


        
      </div>)
      }

      
    </>
  )
}


function Trunk({children}:any){
  const positionRef = useRef<HTMLDivElement>(null)
  useScrollRestoration(useRouter().asPath, positionRef.current!);

  return <div className={styles.trunk} ref={positionRef}>
          <header className={styles.header}>
            {/* <BookSearch bookid={children.props.bookid} lang={children.props.lang} /> */}
          </header>
          <main>
          
            <article className={styles.article}>
              {children}
            </article>
          </main>
          <footer></footer>
        </div>
}
  
