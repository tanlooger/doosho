import {
  useEffect,
  useRef,
  useState,
} from 'react';

import Link from 'next/link';
import { useRouter } from 'next/router';

import styles from './book-sidebar.module.css';

export default function BookSidebar({bookid, bookjson}:any) {
  //const [displayChildren, setDisplayChildren]:any = useState({});
  // const [displayChildren, setDisplayChildren]:any = useReducer(
  //   (state: any, updates: any) => ({ ...state, ...updates }),
  //   {}
  // );
  const [test, setTest]:any = useState({})
  const aspath = useRouter().asPath

  const bookurl = `/${bookjson.slug}`.replace('//', '/')



  useEffect(()=>{
    // var patharr = aspath.split('/').filter(element=>element)

    // while(patharr.length>2){
    //   const ppp = patharr.join('/')



    //   setDisplayChildren({[ppp]:true})
  
    //   patharr.pop()
    // }

    

    window.onload = ()=>{
      // setTimeout(()=>{
      //   const node = document.getElementById(styles.active)
      //   node?.scrollIntoView({ block: 'center' }) 
      // }, 100)
             
    }
  }, [aspath])

  useEffect(()=>{

    // setTimeout(()=>{
    //   const node = document.getElementById(styles.active)
    //   node?.scrollIntoView({ block: 'center' }) 
    // }, 10)
  }, [])


  return (
    <nav className={styles.nav}>
      <Link href={bookurl} scroll={false}><a style={{marginLeft: '1rem'}}  id={useRouter().asPath.trim() === bookurl ? styles.active: bookurl} >{bookjson.title}</a>
      </Link>

      {useRouter().asPath.trim() === bookurl && <input style={{width:1,height:1,border:0,outline:0,zIndex:0}} type="text" autoFocus></input>}
      <MenuList menus={bookjson.child} />
    </nav>
  )
}



function MenuList({ menus }: any){
  if(!menus)return null;
  return (
    <ul style={{marginLeft: '1rem', listStyleType: 'none', paddingLeft:0}}>
      {menus.map((menu: any)=>(<MenuItem menu={menu} key={menu.slug} />))}
    </ul>
  )
}


function MenuItem({menu}: any) {
  const [open, setOpen] = useState(false)
  const active = useRef<HTMLLIElement>(null)

  const url = `/${menu.slug}`.replace('//', '/')
  const aspath = useRouter().asPath
  useEffect(()=>{
    if(aspath.includes(menu.slug) || menu.open){
      setOpen(true)
    }

    // if(aspath===url && active && active.current){
    //   alert(111)
    //   active.current.scrollIntoView({block:'center'})
    // }
    
  }, [aspath, menu.open, menu.slug, url])
  
  if(!menu)return null




  return(menu.child? 
    <li key={menu.slug} ref={active}>
      <span onClick={()=>setOpen(!open)}>{open ? '➖' : '➕'}</span>
      
      <Link href={url} scroll={false}><a title={menu.title}  id={aspath.trim() === url ? styles.active: url}>{menu.title}</a></Link>
      {open && <MenuList menus={menu.child} />}
    </li> 
    : 
    <li key={menu.slug} >
      <Link href={url} scroll={false}><a title={menu.title}  id={aspath === url ? styles.active: url}>✽
      
      {menu.title}</a></Link>

      {aspath === url && <input style={{width:1,height:1,border:0,outline:0,zIndex:0}} type="text" autoFocus></input>}
    </li>
  )
}



